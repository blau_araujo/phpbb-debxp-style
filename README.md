# phpbb-debxp-style

Estilo utilizado no fórum da comunidade debxp.

## Mudanças

- [x] Novos ícones de fóruns e tópicos
- [x] Tipografia
- [x] Voltar para debxp.org
- [ ] Rebranding
- [ ] Relicenciamento

## Futuro

- [ ] Criar uma versão "dark"
